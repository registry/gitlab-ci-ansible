FROM python:2-alpine

ADD ./requirements.txt /requirements.txt
RUN apk -U add gcc libc-dev libffi-dev openssl-dev ca-certificates git \
	&& pip install -r /requirements.txt
